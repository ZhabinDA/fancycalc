﻿namespace FancyCalc
{
    using System;

    using NUnit.Framework;

    [TestFixture]
    public class FancyCalculatorTests
    {
        [TestCase(3, 3, ExpectedResult = 9)]
        [TestCase(1, 0, ExpectedResult = 0)]
        public double MultiplyTest(double a, double b)
        {
            var calc = new FancyCalcEnguine();
            return calc.Multiply(a, b);
        }

        [TestCase(2, 2, ExpectedResult = 4)]
        public double AddTest(double a, double b)
        {
            var calc = new FancyCalcEnguine();
            return calc.Add(a, b);
        }


        [TestCase(2, 2, ExpectedResult = 1)]
        public double DivisionTest(double a, double b)
        {
            var calc = new FancyCalcEnguine();
            return calc.Division(2, 2);
        }

        [TestCase("10 + 20", ExpectedResult = 30)]
        [TestCase("10 - 20", ExpectedResult = -10)]
        [TestCase("5 / 2", ExpectedResult = 2.5)]
        [TestCase("12 * 20", ExpectedResult = 240)]
        [TestCase("12      *         20    ", ExpectedResult = 240)]
        [TestCase("12,5  * 2", ExpectedResult = 25)]
        [TestCase("12.5  * 2", ExpectedResult = 25)]
        public double CulculateTest(string expression)
        {
            var calc = new FancyCalcEnguine();
            return calc.Culculate(expression);
        }

        [Test]
        public void DivisionExceptionTest()
        {
            var calc = new FancyCalcEnguine();
            Assert.Throws<DivideByZeroException>(() => calc.Division(2, 0));
        }

        [Test]
        public void SubtractNegativTest()
        {
            var calc = new FancyCalcEnguine();
            Assert.Throws<ArgumentNullException>(() => calc.Culculate(string.Empty));
            Assert.Throws<ArgumentException>(() => calc.Culculate("4 | 5"));
            Assert.Throws<ArgumentException>(() => calc.Culculate("4 + 5 + 6"));
        }

        [Test]
        public void SubtractTest()
        {
            var calc = new FancyCalcEnguine();
            double expected = 0;
            var actual = calc.Subtract(1, 1);
            Assert.AreEqual(expected, actual);
        }
    }
}