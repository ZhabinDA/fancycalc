﻿namespace FancyCalc
{
    using System;
    using System.Globalization;
    using System.Text.RegularExpressions;

    public class FancyCalcEnguine
    {
        public double Add(double a, double b)
        {
            return a + b;
        }

        public double Culculate(string expression)
        {
            if (string.IsNullOrWhiteSpace(expression))
                throw new ArgumentNullException(nameof(expression));

            const string doublePattern = @"\d+[.|,]?\d*";
            const string operatorPattern = @"\s*(\/|\+|\-|\*)\s*";

            var pattern = @$"{doublePattern}{operatorPattern}{doublePattern}";
            var regex = new Regex(pattern);

            var matchValue = regex.Match(expression).Value;

            if (string.IsNullOrEmpty(matchValue) || !string.IsNullOrWhiteSpace(expression.Replace(matchValue, "")))
                throw new ArgumentException($"Expression not corrected: {expression}");

            var stringWithSeparator = matchValue.Replace(',', '.');
            var values = Regex.Split(stringWithSeparator, operatorPattern);

            if (values.Length != 3)
                throw new ArgumentException($"Expression not corrected: {expression}");

            if (!double.TryParse(values[0], NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out var firstNumbers))
                throw new ArgumentException($"Digital not corrected: {values[0]}");

            if (!double.TryParse(values[2], NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out var secondNumber))
                throw new ArgumentException($"Digital not corrected: {values[2]}");

            switch (values[1])
            {
                case "+": return Add(firstNumbers, secondNumber);
                case "-": return Subtract(firstNumbers, secondNumber);
                case "*": return Multiply(firstNumbers, secondNumber);
                case "/": return Division(firstNumbers, secondNumber);
            }

            throw new ArgumentNullException($"Expression not correct: {expression}");
        }

        public double Division(double a, double b)
        {
            if (b == 0)
                throw new DivideByZeroException();

            return a / b;
        }

        public double Multiply(double a, double b)
        {
            return a * b;
        }

        public double Subtract(double a, double b)
        {
            return a - b;
        }
    }
}